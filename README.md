# "BSD POWER AMP" - Audio Power Amplifier Circuit Design Released Under BSD License

The "BSD POWER AMP" is collection of **original designs** of audio power
amplifier circuits released under the modified "0-Clause License" of BSD
License. BSD POWER AMP focused to the application of class-AB principle and
implementation of BJT (Bipolar Junction Transistor). Some models under BSD
POWER AMP also implement class-TD (Tracking Digital) stepper system to gain
better efficiency and durability, especially these designed to operate with
symmetrical dual power supply voltage at about 100V DC or above. **All
schematics are originally engineered and drawn by the BSD POWER AMP
developer, none of schematics based a reverse engineering of any existing
audio power amplifier products.**

The initial repository of this project is actually on the GITHUB
([https://github.com/heru-himawan-tl/BSD-POWER-AMP](https://github.com/heru-himawan-tl/BSD-POWER-AMP)).
It was moved into new repository at GITLAB since Monday, October
24, 2022.

## About The Name of "BSD POWER AMP"
"BSD" itself is an abbreviation of "Berkeley Software Distribution". However,
"BSD POWER AMP" has nothing to do with Berkeley University of California in
which the Regents of the University of California is the author of BSD
License; it is only a name in which the works are released under BSD License.

## The License

Copyright (C) 2022 by BSD POWER AMP Developer.

Permission to use, copy, modify, and/or distribute BSD POWER AMP schematic for
any purpose with or without fee is hereby granted.

BSD POWER AMP IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS WORKS INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS WORKS.

## Download The Design Bundle

Download the design bundle by follow this hyperlink:

[https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/archive/main/BSD-POWER-AMP-main.zip](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/archive/main/BSD-POWER-AMP-main.zip)

The bundle of design contains schematics in [KiCad](https://www.kicad.org/)
V. 6.x. format, complete with some symbol models & footprint models. 
You can open to view or edit the schematics by using [KiCad EDA](https://www.kicad.org/).

## PCB Design Files

To get the PCB design files (in KiCad format, Gerber format, and PDF format),
please contact the developer by e-mail to: **_the.watcher.from.sky @ gmail.com_**

### PCB for Class-TD Development Step/Release 4 Variants & Consultative Design Releases Variants

![https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-1.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-1.png)

![https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-2.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-2.png)

### PCB for LP-SERIES (Low Power Series) Development Step/Release 2

![https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-3.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-3.png)

## Development Notes

Removed schematics/designs:

- LP-SERIES Development Step/Release 1
- LP-SERIES Development Step/Release 1.1
- 1300 Watts RMS Class-TD Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 5
- 1300 Watts RMS Class-TD Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 6

## Online Schematics

Online schematics are available below. Click on each to get full resolution.
### LP-SERIES 290 Watts RMS Class-H Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 2 (LP-SERIES-DEV-2)


Repository:
[https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/LP-SERIES-DEV-2](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/LP-SERIES-DEV-2)


![LP-SERIES-DEV-2-POWER-AMP.pdf-2022-11-16-14-59-40.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./LP-SERIES-DEV-2/LP-SERIES-DEV-2-POWER-AMP.pdf-2022-11-16-14-59-40.png)


Download or view in PDF: [LP-SERIES-DEV-2-POWER-AMP.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./LP-SERIES-DEV-2/LP-SERIES-DEV-2-POWER-AMP.pdf)


### 1300 Watts RMS Class-TD Audio Power Amplifier for 4-ohms Loudspeaker - Experimental Model Step 2


Repository:
[https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2)


![1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2-POWER-AMP.pdf-2022-11-16-14-59-39.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2-POWER-AMP.pdf-2022-11-16-14-59-39.png)


Download or view in PDF: [1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2-POWER-AMP.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-EXP-2-POWER-AMP.pdf)


### 1300 Watts RMS Class-TD Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 7


Repository:
[https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7)


![1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-POWER-AMP.pdf-2022-11-16-14-59-38.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-POWER-AMP.pdf-2022-11-16-14-59-38.png)


Download or view in PDF: [1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-POWER-AMP.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-POWER-AMP.pdf)


![1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-OVERLOAD-PROTECTOR.pdf-2022-11-16-14-59-37.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-OVERLOAD-PROTECTOR.pdf-2022-11-16-14-59-37.png)


Download or view in PDF: [1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-OVERLOAD-PROTECTOR.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-7-OVERLOAD-PROTECTOR.pdf)


### 1300 Watts RMS Class-TD Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 4


Repository:
[https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4)


![1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-POWER-AMP.pdf-2022-11-16-14-59-37.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-POWER-AMP.pdf-2022-11-16-14-59-37.png)


Download or view in PDF: [1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-POWER-AMP.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-POWER-AMP.pdf)


![1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-OVERLOAD-PROTECTOR.pdf-2022-11-16-14-59-36.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-OVERLOAD-PROTECTOR.pdf-2022-11-16-14-59-36.png)


Download or view in PDF: [1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-OVERLOAD-PROTECTOR.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4/1300-watts-RMS-for-4-ohms-class-TD-audio-power-amp-DEV-4-OVERLOAD-PROTECTOR.pdf)


If you are needing for BSD POWER AMP schematic for greater output power
rating and/or with some advantage features, we open the consultantion service
for you to design it.

To reach our consultantion service, please contact or write to us by e-mail to:

**_the.watcher.from.sky @ gmail.com_**

or contact our official representative via WhatsApp at: +62-813-1720-8524  
