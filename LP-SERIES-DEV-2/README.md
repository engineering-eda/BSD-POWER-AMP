# LP-SERIES 290 Watts RMS Class-H Audio Power Amplifier for 4-ohms Loudspeaker - Development Step/Release 2 (LP-SERIES-DEV-2)

This is "low power series" that may suitable for stage monitor, recording
monitor, and/or for guitar amplifier with full-range loudspeaker. It applies
class-H stepper to reduce the power consumption and so will reduce the heat
of the output-stage power transistors.

This model will provide 290 Watts RMS to handle 4-ohms loudspeaker. The
design is reduced as compact as possible to apply the trough-hole
components to fit within a single-sided-cooper-layer PCB in size of
6.2-inches x 3-inches.

This is **LP SERIES DEV 2**, an update of the previous (removed) LP SERIES.
It has improved class-H stepper.

Features:

- With onboard +/- 15V simple power supply regulator sub-circuit, no need
external +/- 15V power supply to initiate the input-stage amplifier & the
VAS-cascode-couple-amplifier biasing source.

- With High Speed Realtime Bootstrap to pull-up the biasing-voltage for the
source-follower-mode-MOSFET-switch of H-side class-H stepper.

- Simple & compact circuitry of input-stage amplifier and VAS, designed to
suit with the output-stage amplifier that operated with class-H stepper
managed power supply rails.

## The Schematic

![LP-SERIES-DEV-2-POWER-AMP.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./LP-SERIES-DEV-2/LP-SERIES-DEV-2-POWER-AMP.png)

Download or view in PDF: [LP-SERIES-DEV-2-POWER-AMP.pdf](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/./LP-SERIES-DEV-2/LP-SERIES-DEV-2-POWER-AMP.pdf)

Download the schematic bundle: [https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/archive/main/BSD-POWER-AMP-main.zip?path=LP-SERIES-DEV-2](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/archive/main/BSD-POWER-AMP-main.zip?path=LP-SERIES-DEV-2)

## PCB for LP-SERIES (Low Power Series) Development Step/Release 2

![https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-3.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/main-power-amp-pcb-3.png)

To buy the Gerber files of PCB or the physical of PCB, contact the project
maintainer by mail to:

**_the.watcher.from.sky at gmail.com_**

or contact our official representative via WhatsApp at: +62-813-1720-8524  

## ETC

![ss-0.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-0.png)

![ss-1.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-1.png)

![ss-2.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-2.png)

![ss-3.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-3.png)

![ss-4.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-4.png)

![ss-5.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-5.png)

![ss-6.png](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/LP-SERIES-DEV-2/ss-6.png)



