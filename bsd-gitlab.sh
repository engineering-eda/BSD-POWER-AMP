#!/bin/bash

#############################################################################
#                                                                           #
# Script to manage BSD POWER AMP GIT repository                             #
#                                                                           #
# Copyright (C) 2022 Heru Himawan Tejo Laksono.                             #
#                                                                           #
#############################################################################

reset

if [ "$1" = "" ]; then
    exit
fi

DIR=$(realpath $1)

if [ ! -d "$DIR" ]; then
    exit
fi

CWD=$(pwd)
BASE_PRIV="BSD-GITLAB-PRIVATE"
BASE="BSD-GITLAB"
DEST_PRIV="$BASE_PRIV/$(basename $1)"
DEST="$BASE/$(basename $1)"

rm -rf "$DEST_PRIV"
rm -rf "$DEST"

PRIVATE="NO"

for f in $(find $1); do
    is_private=$(echo "$f" | grep 'PRIVATE')
    if [ "$is_private" != "" ] && [ "$PRIVATE" = "NO" ]; then
        PRIVATE="YES"
        echo "=============================================="
        echo "Private repository ..."
        echo "$f"
        echo "=============================================="
    fi
done

if [ ! -d "$DEST_PRIV" ]; then
    mkdir -p $DEST_PRIV
fi

if [ "$PRIVATE" = "NO" ]; then
    if [ ! -d "$DEST" ]; then
        mkdir -p $DEST
    fi
    for f in $(find $1); do
        hf=$(echo "$f" | awk '/(\.(gbr|gvp|kicad_pcb|drl|ps|git|raw|log|pos|csv)|preview|report\.txt|.*-bak|.*-backup.*|.*\.bak|ANALYSIS.*\.net|.*\.kicad_sch-.*|.*auto_saved.*|.*-F_.*|.*-B_.*|.*drl.*|.*_Drawings.*|.*_Eco.*|.*_Cuts.*|.*-Margin.*|.*pcb.*)/')
        if [ "$hf" = "" ]; then
            dn=$(dirname $f)
            if [ -d "$f" ]; then
                mkdir -p "$BASE/$f"
            fi
            if [ -f "$f" ]; then
                cp -af "$f" $BASE/$dn/.
            fi
        fi
    done
fi

for f in $(find $1); do
    hf=$(echo "$f" | awk '/(.*-bak|.*\.bak|ANALYSIS.*\.net|.*-backup.*|.*\.kicad_sch-.*|.*auto_saved.*|.*\.raw|.*\.log)/')
    if [ "$hf" = "" ]; then
        dn=$(dirname $f)
        if [ -d "$f" ]; then
            mkdir -p "$BASE_PRIV/$f"
        fi
        if [ -f "$f" ]; then
            cp -af "$f" $BASE_PRIV/$dn/.
        fi
    fi
done

cd $BASE_PRIV

git add .
git commit -m "Update `date`"
git push -u origin main

cd $CWD

if [ "$PRIVATE" = "YES" ]; then
    exit
fi

cd $BASE

CURRENT_DIR=

if [ -f ../BSD-POWER-AMP-README.md ]; then
    cat ../BSD-POWER-AMP-README.md > README.md
    SCH_PNG=
    for f in $(find . -type f -name '*.pdf' -printf '%f\t%p\n' | sort | cut -d$'\t' -f2); do
        is_git=$(echo "$f" | awk '/.*\.git.*/')
        if [ "$is_git" = "" ]; then
            pdf=$(echo "$f" | awk '/.*(POWER-AMP|OVERLOAD-PROTECTOR)\.pdf$/')
            if [ "$pdf" != "" ]; then
                echo "============================================="
                echo "Found power amplifier schematic in PDF: $f"
                echo "============================================="
                rm -f $f*.png
                pdftocairo -png $f $f
                nf="$f-$(date "+%Y-%m-%d-%H-%M-%S").png"
                mv $f-1.png $nf
                mainsch=$(echo "$f" | awk '/.*POWER-AMP\.pdf$/')
                INFOTXT=
                CURRENT_DIR_TMP=$(dirname $f)
                CURRENT_DIR_CHK=$(echo $CURRENT_DIR_TMP | awk '/(\/.*\/.*|.*\.git.*)$/')
                if [ "$CURRENT_DIR_CHK" = "" ] && [ "$CURRENT_DIR_TMP" != "." ] && [ "$mainsch" != "" ]; then
                    if [ "$CURRENT_DIR" != "$CURRENT_DIR_TMP" ]; then
                        echo $CURRENT_DIR_TMP
                        CURRENT_DIR="$CURRENT_DIR_TMP"
                        repository="https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/tree/main/$(basename $CURRENT_DIR)"
                        INFOTXT="$CURRENT_DIR/INFO.txt Repository: [$repository]($repository)"
                    fi
                fi
                cfpng="![$(basename $nf)](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/$nf)"
                cfpdf="$f"
                SCH_PNG="$INFOTXT $cfpng $cfpdf $SCH_PNG"
            fi
        fi
    done
    for png in $(echo $SCH_PNG); do
        infotxt=$(echo "$png" | awk '/.*INFO\.txt$/')
        pdffile=$(echo "$png" | awk '/.*\.pdf$/')
        if [ "$pdffile" != "" ]; then
            echo "Download or view in PDF: [$(basename $png)](https://gitlab.com/engineering-eda/BSD-POWER-AMP/-/raw/main/$png)" >> README.md
        else
            if [ "$infotxt" != "" ]; then
                echo "### $(cat $png)" >> README.md
            else
                echo "$png" >> README.md
            fi
        fi
        pd_repository=$(echo "$png" | awk '/.*Repository:.*/')
        if [ "$pd_repository" = "" ]; then
            echo -e "\n" >> README.md
        fi
    done
fi

cat ../BSD-POWER-AMP-README-FOOTER >> README.md

cp -af ../bsd-gitlab.sh .

git add .
git commit -m "Update `date`"
git push -u origin main

cd $CWD

exit
