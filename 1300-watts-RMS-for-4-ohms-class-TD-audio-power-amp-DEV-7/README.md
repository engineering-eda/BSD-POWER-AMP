# 1300 Watts RMS Class-TD Audio Power Amplifier For 4-Ohms Loudspeaker

This is development release 7 of class-TD (tracking digital) of "BSD POWER AMP"
audio power amplifier design. This model designed to yield at about 1300 watts
RMS for handling loudspeakers in 4-ohms impedance.
